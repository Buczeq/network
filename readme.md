# Network

[PL]
Finalna wersja projektu na przedmiot "Sieci komputerowe w języku java".
Zakłada stworzenie prostej sieci opartej o protokoły TCP i UDP.
Pełny opis znajduje się w pliku opis.txt

Wymagania obejmowały brak pakietów oraz używania bibliotek.

[ENG]
Final version of the project for the subject "Computer networks in java".
It assumes the creation of a simple network based on TCP and UDP protocols.

Requirements included no packages and no libraries to be used.
