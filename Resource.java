class Resource {
    RESOURCETYPE type;
    Integer clientId;
    boolean lock;
    String nodeIp;
    Integer nodePort;

    public Resource(RESOURCETYPE type, Integer clientId, boolean lock, String nodeIp, Integer nodePort) {
        this.type = type;
        this.clientId = clientId;
        this.lock = lock;
        this.nodeIp = nodeIp;
        this.nodePort = nodePort;
    }

    /**
     * Tworzy pusty zasób danego typu
     */
    public Resource(RESOURCETYPE type) {
        this.type = type;
        this.clientId = null;
        this.lock = false;
        this.nodeIp = null;
        this.nodePort = null;
    }

    String convertToString() {
        String result = "";
        result += type + " " + clientId + " " + lock + " " + nodeIp + " " + nodePort;

        return result;
    }


    public RESOURCETYPE getType() {
        return type;
    }

    public Integer getNodePort() {
        return nodePort;
    }

}