import java.io.*;
import java.net.ServerSocket;
import java.util.*;

public class NetworkNode {
    String ip = "localhost";
    String gatewayIp = null;
    int gatewayPort = 0;
    boolean headNode = true;
    int port = 0;
    String identifier = null;
    List<Resource> resources = new ArrayList<>();
    ServerSocket serverSocket;
    ConnectionTCP gatewayConnection;
    ConnectionUDP udpConnection;
    List<ConnectionTCP> connections = new ArrayList<>();
    List<NodeInfo> childrenUDP = new ArrayList<>();
    boolean udp = false;
    NodeInfo parentInfoUDP;

    public static void main(String[] args) throws IOException {
        NetworkNode node = new NetworkNode();
        node.resolveArgs(args);
        if (node.udp) {
            UDPService.handleUDP(node);
        } else {
            TCPService.gateway(node);
        }
        TCPService.listen(node);

    }

    void resolveArgs(String[] args) {
        for (int i = 0; i < args.length; i++) {
            switch (args[i]) {
                case "-ident":
                    identifier = args[++i];
                    break;
                case "-tcpport":
                    port = Integer.parseInt(args[++i]);
                    break;
                case "-udpport":
                    port = Integer.parseInt(args[++i]);
                    udp = true;
                    break;
                case "-gateway":
                    String[] gatewayArray = args[++i].split(":");
                    gatewayIp = gatewayArray[0];
                    gatewayPort = Integer.parseInt(gatewayArray[1]);
                    headNode = false;
                    break;
                default:
                    if (args[i].contains(":")) {
                        String[] resourcesArray = args[i].split(":");
                        RESOURCETYPE type = RESOURCETYPE.valueOf(resourcesArray[0]);
                        for (int j = 0; j < Integer.parseInt(resourcesArray[1]); j++) {
                            resources.add(new Resource(type));
                        }
                    }
            }
        }
    }

}
