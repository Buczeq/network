import java.io.*;
import java.net.Socket;
import java.util.*;
import java.util.stream.Collectors;

public class ConnectionTCP extends Connection {
    Socket socket;
    NetworkNode node;
    BufferedReader br;
    BufferedWriter bw;
    String gatewayIdent;
    String parentIdent;

    ConnectionTCP(Socket socket, NetworkNode node) throws IOException {
        this.socket = socket;
        this.node = node;
        this.br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    }

    /**
     * Nasłuchuje poleceń od klientów lub innych node-ów, a następnie je obsługuje.
     */
    @Override
    public void run() {
        try {
            String line;
            while ((line = br.readLine()) != null) {
                String[] words = Service.getWordsFromString(line);
                switch (words[0]) {
                    case "NODE": {
                        handleNewNode(words);
                        break;
                    }
                    case "TERMINATE": {
                        if (node.udp) {
                            UDPService.terminateUDP(node);
                        }
                        terminate();
                        return;
                    }
                    case "LOCK": {
                        Client client = Service.createClientWithResources(words);
                        handleLock(client);
                        break;
                    }
                    case "UNLOCK": {
                        Client client = Service.createClientWithResources(words);
                        unlockResources(client);
                        break;
                    }
                    case "true":
                    case "false": {
                        break;
                    }
                    default: {
                        handleNetworkClient(words);
                        return;
                    }
                }
            }
        } catch (IOException ignored) {
        }
    }

    /**
     * Zamyka socket który wysłał polecenie wyłączenia sieci, a następnie wyłącza sieć.
     */
    private void terminate() throws IOException {
        if (!socket.isClosed()) {
            socket.close();
        }
        TCPService.terminate(node);
        if (!node.serverSocket.isClosed()) {
            node.serverSocket.close();
        }
    }

    /**
     * Odblokowuje zasoby klientów, którym nie udało się zablokować wszystkich wymaganych zasobów.
     */
    public void unlockResources(Client client) throws IOException {
        String result = Service.unlockResources(client, node, Service.convertResourcesToString(client.resources));
        for (ConnectionTCP connection : node.connections) {
            if (!connection.socket.isClosed() && (client.gatewayIdent == null || !Objects.equals(connection.gatewayIdent, client.gatewayIdent))) {
                TCPService.sendTcpMessage(connection.bw, result);
            }
        }
    }

    @Override
    public boolean lockInChildren(Client client) throws IOException {
        boolean success = false;
        for (ConnectionTCP connection : node.connections) {
            if (!connection.socket.isClosed() && (client.gatewayIdent == null || !Objects.equals(connection.gatewayIdent, client.gatewayIdent))) {
                if (success) {
                    break;
                }
                TCPService.sendTcpMessage(connection.bw,
                        "LOCK " + node.identifier + " " + Service.convertResourcesToString(client.resources));
                String[] words = Service.getWordsFromString(connection.br.readLine());
                Service.applyResources(client, words);
                if (Boolean.parseBoolean(words[0])) {
                    success = true;
                }
            }
        }
        return success;
    }

    @Override
    public boolean lockInParent(Client client) throws IOException {
        if (node.headNode || (client.gatewayIdent != null && client.gatewayIdent.equals(parentIdent))) {
            return false;
        }
        TCPService.sendTcpMessage(node.gatewayConnection.bw,
                "LOCK " + node.identifier + " " + Service.convertResourcesToString(client.resources));
        String[] words = Service.getWordsFromString(node.gatewayConnection.br.readLine());
        Service.applyResources(client, words);
        return Boolean.parseBoolean(words[0]);
    }

    /**
     * Obsługuje żądanie zablokowania zasobów wysłane przez inny node.
     */
    public void handleLock(Client client) throws IOException {
        String result = lockResources(client, node) + " " + Service.convertResourcesToString(client.resources);
        TCPService.sendTcpMessage(bw, result);
        TCPService.sendTcpMessage(bw, result);
    }

    /**
     * Dodaje połączenie jako swoje dziecko i wysyła mu swój identyfikator.
     */
    private void handleNewNode(String[] words) throws IOException {
        gatewayIdent = words[1];
        TCPService.sendTcpMessage(bw, node.identifier);
        node.connections.add(this);
    }

    /**
     * Przyjmuje żądanie zablokowania zasobów w sieci. Blokuje je za pomocą protokołu UDP lub TCP (w zależności od flagi
     * wywołania - odpowiednio -udpport i -tcpport). Jeśli operacja zablokowania się powiedzie, wysyła raport w postaci
     * seri wierszy: <zasób>:<liczność>:<ip węzła>:<port węzła>
     * Jeśli nie to odblokowuje zasoby w sieci wysyłając polecenie UNLOCK i zwraca komunikat FAILED do klienta.
     * Następnie kończy połączenie.
     */
    private void handleNetworkClient(String[] words) throws IOException {
        Client client = new Client(words);
        boolean isLocked;
        if (node.udp) {
            isLocked = node.udpConnection.lockResources(client, node);
        } else {
            isLocked = lockResources(client, node);
        }
        if (isLocked) {
            Set<Pair> pairs2 = client.resources.stream().map(resource -> new Pair(resource.getNodePort(), resource.getType())).collect(Collectors.toSet());
            for (Pair i : pairs2) {
                long amount = client.resources.stream().filter(resource -> resource.type == i.type && Objects.equals(resource.nodePort, i.port)).count();
                String message = i.type + ":" + amount + ":" + node.ip + ":" + i.port;
                TCPService.sendTcpMessage(bw, message);
            }
        } else {
            if (node.udp) {
                node.udpConnection.unlockResources(client);
            } else {
                unlockResources(client);
            }
            TCPService.sendTcpMessage(bw, "FAILED");
        }

        socket.close();
    }

}
