import java.util.*;

public class Client {
    int clientId;
    List<Resource> resources = new ArrayList<>();
    String gatewayIdent;


    /**
     * Tworzy obiekt klienta
     * @param words komunikat od klienta w formacie <identyfikator> <zasób>:<liczność> [<zasób>:liczność]
     */
    Client(String[] words) {
        this.clientId = Integer.parseInt(words[0]);
        for (int i = 1; i < words.length; i++) {
            if (words[i].contains(":")) {
                String[] resourcesArray = words[i].split(":");
                for (int j = 0; j < Integer.parseInt(resourcesArray[1]); j++) {
                    this.resources.add(new Resource(RESOURCETYPE.valueOf(resourcesArray[0]), this.clientId, false, null, null));
                }
            }
        }
    }

    Client() {
    }

}