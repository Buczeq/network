#!/bin/bash
javac Client.java
javac Connection.java
javac ConnectionTCP.java
javac ConnectionUDP.java
javac NetworkClient.java
javac NetworkNode.java
javac NodeInfo.java
javac Pair.java
javac Resource.java
javac RESOURCETYPE.java
javac Service.java
javac TCPService.java
javac UDPService.java