import java.net.InetAddress;

/**
 * Zawiera informacje o dziecku lub rodzicu (protokół udp)
 */
public class NodeInfo {
    String gatewayIdent;
    String parentIdent;
    InetAddress senderAddress;
    Integer senderPort;
}
