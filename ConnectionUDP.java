
import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.*;

public class ConnectionUDP extends Connection {
    DatagramSocket socket;
    NetworkNode node;

    ConnectionUDP(DatagramSocket socket, NetworkNode node) {
        this.socket = socket;
        this.node = node;
    }

    /**
     * Nasłuchuje poleceń od innych node-ów, a następnie je obsługuje.
     */
    @Override
    public void run() {
        try {
            while (true) {
                DatagramPacket packet = new DatagramPacket(new byte[2048], 2048);
                socket.receive(packet);
                String message = new String(packet.getData(), 0, packet.getLength()).replace("\u0000", "");
                String[] words = Service.getWordsFromString(message);
                switch (words[0]) {
                    case "NODE": {
                        handleNewNode(words, packet);
                        break;
                    }
                    case "TERMINATE": {
                        UDPService.terminateUDP(node);
                        if (!socket.isClosed()) {
                            socket.close();
                        }
                        return;
                    }
                    case "true":
                    case "false": {
                        handleRepeatCommand(packet);
                        break;
                    }
                    case "LOCK": {
                        handleLock(words, packet);
                        break;
                    }
                    case "UNLOCK": {
                        Client client = Service.createClientWithResources(words);
                        unlockResources(client);
                        break;
                    }
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Wysyła informację o konieczności powtórzenia polecenia.
     */
    private void handleRepeatCommand(DatagramPacket packet) throws IOException {
        String mes = "AGAIN";
        DatagramPacket packetResponse = new DatagramPacket(
                mes.getBytes(),
                mes.getBytes().length,
                packet.getAddress(),
                packet.getPort()
        );
        socket.send(packetResponse);
    }

    @Override
    public boolean lockInChildren(Client client) throws IOException {
        boolean success = false;
        for (NodeInfo child : node.childrenUDP) {
            if ((client.gatewayIdent == null || !Objects.equals(child.parentIdent, client.gatewayIdent))) {
                if (success) {
                    break;
                }
                String message = "LOCK " + node.identifier + " " + Service.convertResourcesToString(client.resources);
                DatagramPacket packet = new DatagramPacket(
                        message.getBytes(),
                        message.getBytes().length,
                        child.senderAddress,
                        child.senderPort
                );
                socket.send(packet);
                DatagramPacket packetToReceive = new DatagramPacket(new byte[2048], 2048);
                socket.receive(packetToReceive);
                String m = new String(packetToReceive.getData(), 0, packetToReceive.getLength()).replace("\u0000", "");
                String ok = "OK";
                DatagramPacket okPacket = new DatagramPacket(
                        ok.getBytes(),
                        ok.getBytes().length,
                        child.senderAddress,
                        child.senderPort
                );
                socket.send(okPacket);
                String[] words = Service.getWordsFromString(m);
                Service.applyResources(client, words);
                if (Boolean.parseBoolean(words[0])) {
                    success = true;
                }
            }
        }
        return success;
    }

    @Override
    public boolean lockInParent(Client client) throws IOException {
        if (node.headNode || (client.gatewayIdent != null && client.gatewayIdent.equals(node.parentInfoUDP.parentIdent))) {
            return false;
        }
        String message = "LOCK " + node.identifier + " " + Service.convertResourcesToString(client.resources);
        DatagramPacket packet = new DatagramPacket(
                message.getBytes(),
                message.getBytes().length,
                node.parentInfoUDP.senderAddress,
                node.parentInfoUDP.senderPort
        );
        socket.send(packet);

        DatagramPacket packetToReceive = new DatagramPacket(new byte[2048], 2048);
        socket.receive(packetToReceive);
        String m = new String(packetToReceive.getData(), 0, packetToReceive.getLength()).replace("\u0000", "");
        String ok = "OK";
        DatagramPacket okPacket = new DatagramPacket(
                ok.getBytes(),
                ok.getBytes().length,
                node.parentInfoUDP.senderAddress,
                node.parentInfoUDP.senderPort
        );
        socket.send(okPacket);
        String[] words = Service.getWordsFromString(m);
        Service.applyResources(client, words);


        return Boolean.parseBoolean(words[0]);
    }

    /**
     * Odblokowuje zasoby klientów, którym nie udało się zablokować wszystkich wymaganych zasobów.
     */
    public void unlockResources(Client client) throws IOException {
        String result = Service.unlockResources(client, node, Service.convertResourcesToString(client.resources));
        for (NodeInfo info : node.childrenUDP) {
            if ((client.gatewayIdent == null || !Objects.equals(info.parentIdent, client.gatewayIdent))) {
                DatagramPacket packet = new DatagramPacket(
                        result.getBytes(),
                        result.getBytes().length,
                        info.senderAddress,
                        info.senderPort
                );
                socket.send(packet);
            }

        }
    }
    /**
     * Obsługuje żądanie zablokowania zasobów wysłane przez inny node.
     * Wysyła najnowszy status klienta wraz z informacjami o jego zasobach.
     * W przypadku konieczności powtórzenia polecenia (otrzymanie informacji AGAIN) powtarza je.
     */
    private void handleLock(String[] words, DatagramPacket packet) throws IOException {
        Client client = Service.createClientWithResources(words);
        String result = lockResources(client, node) + " " + Service.convertResourcesToString(client.resources);
        DatagramPacket packetResponse = new DatagramPacket(
                result.getBytes(),
                result.getBytes().length,
                packet.getAddress(),
                packet.getPort()
        );
        socket.send(packetResponse);
        DatagramPacket packet2 = new DatagramPacket(new byte[2048], 2048);
        socket.receive(packet2);
        String msg = new String(packet2.getData(), 0, packet2.getLength()).replace("\u0000", "");
        if (msg.equals("AGAIN")) {
            socket.send(packetResponse);
        }
    }

    /**
     * Dodaje połączenie jako swoje dziecko i wysyła mu swój identyfikator.
     */
    private void handleNewNode(String[] words, DatagramPacket packet) throws IOException {
        NodeInfo nodeInfo = new NodeInfo();
        nodeInfo.gatewayIdent = words[1];
        nodeInfo.senderAddress = packet.getAddress();
        nodeInfo.senderPort = packet.getPort();
        node.childrenUDP.add(nodeInfo);
        packet = new DatagramPacket(
                node.identifier.getBytes(),
                node.identifier.getBytes().length,
                nodeInfo.senderAddress,
                nodeInfo.senderPort);
        socket.send(packet);
    }
}
