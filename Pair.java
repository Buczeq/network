public class Pair{
    Integer port;
    RESOURCETYPE type;

    public Pair(Integer nodePort, RESOURCETYPE type) {
        this.port = nodePort;
        this.type = type;
    }
}