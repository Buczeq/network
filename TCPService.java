import java.io.BufferedWriter;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPService extends Service {

    /**
     * Uruchamia połączenie z rodzicem w nowym wątku. Wysyła mu polecenie NODE wraz ze swoim identyfikatorem oraz nasłuchuje
     * identyfikatora rodzica.
     */
    public static void gateway(NetworkNode node) throws IOException {
        if (node.headNode) {
            return;
        }
        Socket socket = new Socket(node.gatewayIp, node.gatewayPort);
        node.gatewayConnection = new ConnectionTCP(socket, node);
        TCPService.sendTcpMessage(node.gatewayConnection.bw, "NODE " + node.identifier);
        node.gatewayConnection.parentIdent = node.gatewayConnection.br.readLine();
        node.gatewayConnection.start();
    }

    /**
     * Nasłuchuje nowych połączeń TCP od klientów lub innych nodeów i każde z nich uruchamia w nowym wątku
     */
    public static void listen(NetworkNode node) throws IOException {
        node.serverSocket = new ServerSocket(node.port);
        while (true) {
            try {
                new ConnectionTCP(node.serverSocket.accept(), node).start();
            } catch (IOException e) {
                terminate(node);
                break;
            }
        }
    }

    /**
     * Wysyła polecenie TERMINATE swoim dzieciom i rodzicowi.
     */
    public static void terminate(NetworkNode node) throws IOException {
        if (node.gatewayConnection != null && !node.gatewayConnection.socket.isClosed()) {
            TCPService.sendTcpMessage(node.gatewayConnection.bw, "TERMINATE");
            node.gatewayConnection.socket.close();
        }
        for (ConnectionTCP connection : node.connections) {
            if (!connection.socket.isClosed()) {
                TCPService.sendTcpMessage(connection.bw, "TERMINATE");
                connection.socket.close();
            }
        }
    }

    /**
     * Wysyła wiadomość protokołem TCP
     */
    public static void sendTcpMessage(BufferedWriter bw, String msg) throws IOException {
        bw.write(msg);
        bw.newLine();
        bw.flush();
    }

}
