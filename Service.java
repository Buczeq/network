import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Service {
    /**
     * Tworzy klienta wraz z informacjami o jego zasobach.
     * @param words informacje w postaci <COMMAND> <IDENT> <TYP_ZASOBU> <CLIENT_ID> <CZY_ZABLOKOWANO> <IP_NODE> <PORT_NODE>
     */
    public static Client createClientWithResources(String[] words) {
        Client client = new Client();
        for (int i = 2; i < words.length; i++) {
            client.resources.add(new Resource(
                    RESOURCETYPE.valueOf(words[i]),
                    Integer.parseInt(words[i + 1]),
                    Boolean.parseBoolean(words[i + 2]),
                    !Objects.equals(words[i + 3], "null") ? words[i + 3] : null,
                    !Objects.equals(words[i + 4], "null") ? Integer.parseInt(words[i + 4]) : null));
            i = i + 4;
        }
        client.clientId = client.resources.get(0).clientId;
        client.gatewayIdent = words[1];
        return client;
    }

    /**
     * Zamienia text na tablicę słów.
     */
    public static String[] getWordsFromString(String text) {
        String[] words = text.split("\\s+");
        for (int i = 0; i < words.length; i++) {
            words[i] = words[i].replace(" ", "");
        }
        return words;
    }

    /**
     * Odblokowuje zasoby w nodzie na podstawie id klienta i zwraca polecenie UNLOCK
     */
    public static String unlockResources(Client client, NetworkNode node, String resources) {
        node.resources.stream()
                .filter(r -> r.clientId != null)
                .filter(r -> r.clientId == client.clientId)
                .forEach(r -> {
                    r.lock = false;
                    r.clientId = null;
                });
        return "UNLOCK " + node.identifier + " " + resources;
    }

    /**
     * Konwertuje listę zasobów na tekst.
     */
    public static String convertResourcesToString(List<Resource> resources) {
        StringBuilder result = new StringBuilder();
        for (Resource resource : resources) {
            result.append(resource.convertToString()).append(" ");
        }
        result.setLength(result.length() - 1);
        return result.toString();
    }

    /**
     * Nadpisuje zasoby klienta
     */
    public static void applyResources(Client client, String[] words) {
        List<Resource> resourcesResult = new ArrayList<>();
        for (int i = 1; i < words.length; i++) {
            resourcesResult.add(new Resource(
                    RESOURCETYPE.valueOf(words[i]),
                    Integer.parseInt(words[i + 1]),
                    Boolean.parseBoolean(words[i + 2]),
                    !Objects.equals(words[i + 3], "null") ? words[i + 3] : null,
                    !Objects.equals(words[i + 4], "null") ? Integer.parseInt(words[i + 4]) : null));
            i = i + 4;
        }
        client.resources = resourcesResult;
    }
}
