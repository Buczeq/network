import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class Connection extends Thread {
    /**
     * Blokuje zasoby w nodzie. Jeśli się nie uda to próbuje zablokować u rodzica lub dzieci.
     *
     * @param client Klient zawierający informacje które zasoby muszą być zablokowane
     * @param node   Node w którym należy zablokować zasoby
     * @return Informacja czy udało się zablokować wszystkie zasoby
     */
    boolean lockResources(Client client, NetworkNode node) throws IOException {
        AtomicBoolean successfullyLocked = new AtomicBoolean(true);
        client.resources
                .stream()
                .filter(r -> !r.lock)
                .filter(r -> r.nodePort == null)
                .forEach(cr -> {
                    Optional<Resource> nodeResourceOptional = node.resources.stream()
                            .filter(nr -> !nr.lock)
                            .filter(nr -> nr.clientId == null)
                            .filter(nr -> nr.type == cr.type)
                            .findFirst();
                    if (nodeResourceOptional.isPresent()) {
                        nodeResourceOptional.get().lock = true;
                        nodeResourceOptional.get().clientId = cr.clientId;
                        cr.lock = true;
                        cr.nodePort = node.port;
                        cr.nodeIp = node.ip;
                    } else {
                        successfullyLocked.set(false);
                    }

                });
        if (successfullyLocked.get()) {
            return true;
        } else {
            if (lockInParent(client)) {
                return true;
            } else {
                return lockInChildren(client);
            }
        }

    }

    /**
     * Blokuje zasoby wśród swoich dzieci. Wysyła im polecenie LOCK i otrzymuje najnowszy status klienta wraz z informacją,
     * które z jego zasobów są już zablokowane.
     * @param client Klient, dla którego zasoby mają być zablokowane
     * @return Informacja czy udało się zablokować wszystkie zasoby
     */
    public abstract boolean lockInChildren(Client client) throws IOException;

    /**
     * Blokuje zasoby u swojego rodzica. Wysyła mu polecenie LOCK i otrzymuje najnowszy status klienta wraz z informacją,
     * które z jego zasobów są już zablokowane.
     * @param client Klient, dla którego zasoby mają być zablokowane
     * @return Informacja czy udało się zablokować wszystkie zasoby
     */
    public abstract boolean lockInParent(Client client) throws IOException;
}
