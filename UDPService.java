import java.io.IOException;
import java.net.*;

public class UDPService extends Service {

    /**
     * Uruchamia protokół UDP. Jeśli może łączy się do rodzica, wysyła mu swój identyfikator oraz oczekuje identyfikatora
     * rodzica. Uruchamia nasłuchiwanie poleceń w nowym wątku.
     */
    public static void handleUDP(NetworkNode node) {
        try {
            DatagramSocket socket = new DatagramSocket(node.port);
            node.udpConnection = new ConnectionUDP(socket, node);
            if (!node.headNode) {
                String message = "NODE " + node.identifier;
                DatagramPacket packet = new DatagramPacket(
                        message.getBytes(),
                        message.getBytes().length,
                        InetAddress.getByName(node.gatewayIp),
                        node.gatewayPort
                );
                socket.send(packet);

                DatagramPacket packetToReceive = new DatagramPacket(new byte[2048], 2048);
                socket.receive(packetToReceive);
                String m = new String(packetToReceive.getData(), 0, packetToReceive.getLength()).replace("\u0000", "");

                node.parentInfoUDP = new NodeInfo();
                node.parentInfoUDP.parentIdent = m;
                node.parentInfoUDP.gatewayIdent = m;
                node.parentInfoUDP.senderPort = packetToReceive.getPort();
                node.parentInfoUDP.senderAddress = packetToReceive.getAddress();
            }
            node.udpConnection.start();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Zamyka sieć połączeń UDP z innymi wątkami.
     * Wysyła dzieciom i rodzicowi polecenie TERMINATE.
     * @param node
     */
    public static void terminateUDP(NetworkNode node) {
        try {
            String message = "TERMINATE";
            DatagramSocket socket;
            if (!node.udpConnection.socket.isClosed()) {
                socket = node.udpConnection.socket;

                if (!node.headNode) {
                    DatagramPacket packet = new DatagramPacket(
                            message.getBytes(),
                            message.getBytes().length,
                            InetAddress.getByName(node.gatewayIp),
                            node.gatewayPort
                    );
                    socket.send(packet);
                }
                if (!socket.isClosed()) {
                    socket.close();
                }
            }
            if (!node.udpConnection.socket.isClosed()) {
                socket = node.udpConnection.socket;
            } else {
                socket = new DatagramSocket(node.port);
            }
            for (NodeInfo info : node.childrenUDP) {
                DatagramPacket packet = new DatagramPacket(
                        message.getBytes(),
                        message.getBytes().length,
                        info.senderAddress,
                        info.senderPort
                );
                socket.send(packet);
            }

            if (!node.udpConnection.socket.isClosed()) {
                node.udpConnection.socket.close();
            }
            if (!node.serverSocket.isClosed()) {
                node.serverSocket.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
